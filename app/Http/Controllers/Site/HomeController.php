<?php

namespace App\Http\Controllers\Site;

use App\Models\Pet;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function home(){

        $pets = Pet::limit(8)->get();
       return view('home.index',['pets'=>$pets]);
    }

    public function ongs(){
        return view('home.ongs');
    }

    public function causa(){
        return view('home.causa');
    }

    public function doacao(){
        return view('home.doacao');
    }


     public function petAuau(){
        return view('pet.saudeAuau');
    }

    public function petMiau(){
        return view('pet.saudeMiau');
    }

    public function petShop(){
        return view('pet.petShop');
    }

    public function visualizarpet(){
        return view('pet.visualizarpet');
    }

    public function conheca(){
        return view('home.conheca');
    }

    public function quemsomos(){
        return view('home.quemsomos');
    }




    public function voluntarios(){
        return view('home.voluntarios');
    }


}
