<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Pet;
use Illuminate\Http\Request;

class PetController extends Controller
{
    public function listarPet(){
     $pets = Pet::all();
        $dadosPets = $pets->map(function($pet){
            $pet->imagem = url($pet->imagem );
            return $pet;
        });
        return response()->json($dadosPets);

    }
}
