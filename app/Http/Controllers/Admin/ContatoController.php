<?php

namespace App\Http\Controllers\Admin;

use App\Models\Contato;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class ContatoController extends Controller
{
    public function index()
    {
        $contatos = Contato::paginate(15);
        return view('admin.contatos.index', [
            'contatos' => $contatos
        ]);
    }

    public function create()
    {

        return view('admin.contatos.cadastrar', [
            'contatos' => new Contato()
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'nome' =>'required',
            'email' => 'required',
            'telefone' => 'required',
            'mensagem' => 'required',

        ]);

        $contato = new Contato();
        $contato->nome = $request->nome;
        $contato->email =$request->email;


        $contato->save();

        return redirect()->route('admin.contatos.index')->with('sucesso', 'Contatos Cadastrado com Sucesso');

    }

    public function edit($id)
    {
        $contato = Contato::findOrFail($id);

        return view('admin.contatos.editar', [
            'contato' => $contato
        ]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nome' =>'required',
            'email' => 'required',
            'telefone' => 'required',
            'mensagem' => 'required',

        ]);

        $contato = Contato::findOrFail();
        $contato->nome = $request->nome;
        $contato->email =$request->email;


        $contato->save();

        return redirect()->route('admin.contatos.index')->with('sucesso', 'Contatos Atualizado com Sucesso');

    }

    public function destroy($id)
    {
        $contato = Contato::findOrFail($id);

        if($contato->delete()){

            Storage::delete('public/contatos' . basename($contato->contact));

            return redirect()->route('admin.contatos.index')->with('sucesso', 'Contato Excluido com Sucesso!!');
        }else{

            return redirect()->route('admin.contatos.editar')->with('erro', 'Houve um erro ao Excluir o registro');

        }

    }



}
