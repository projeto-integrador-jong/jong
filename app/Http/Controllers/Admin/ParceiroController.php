<?php

namespace App\Http\Controllers\Admin;

use App\Models\Parceiro;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;


class ParceiroController extends Controller
{

    public function index()
    {
        $parceiros = Parceiro::paginate(15);
        return view('admin.parceiros.index',[
            'parceiros'=> $parceiros
        ]);

    }

    public function create()
    {
        return view('admin.parceiros.cadastrar',[
            'parceiro' => new Parceiro()
        ]);

    }

    public function store(Request $request)
    {
        $request->validate([
            'nome' =>'required',
            'imagem' => 'required',
            'link' => 'required',

        ]);

        $parceiro = new Parceiro();
        $parceiro->nome = $request->nome;

        $parceiro->link =$request->link;

        if($request->hasFile('imagem')){
            $imagemPath = $request->file('imagem')->store('public/parceiros');
            $parceiro->imagem = Storage::url($imagemPath);

        }
        $parceiro->save();
        return redirect()->route('admin.parceiros.index')->with('sucesso', 'Parceiros Cadastrado com Sucesso');

    }

    public function edit($id)
    {
        $parceiro = Parceiro::findOrFail($id);

        return view('admin.parceiros.editar', [
            'parceiro' => $parceiro
        ]);


    }

    public function update(Request $request, $id)
    {

        $request->validate([
            'nome' =>'required',
            'imagem' => 'required',
            'link' => 'required',

        ]);

        $parceiro = new Parceiro();
        $parceiro->nome = $request->nome;

        $parceiro->link =$request->link;

        if($request->hasFile('imagem')){
            $imagemPath = $request->file('imagem')->store('public/parceiros');
            $parceiro->imagem = Storage::url($imagemPath);

        }
        $parceiro->save();
        return redirect()->route('admin.parceiros.index')->with('sucesso', 'Parceiros Cadastrado com Sucesso');


    }

    public function destroy($id)
    {
        $parceiro = Parceiro::findOrFail($id);

        if($parceiro->delete()){

            Storage::delete('public/parceiros' . basename($parceiro->parceiros));

            return redirect()->route('admin.parceiros.index')->with('sucesso', 'Parceiro Excluido com Sucesso!!');
        }else{

            return redirect()->route('admin.parceiros.editar')->with('erro', 'Houve um erro ao Excluir o registro');

        }

    }


}
