<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class UsuarioController extends Controller
{

    public function index()
    {
        $usuario =  User::paginate(15);
        return view('admin.usuarios.index',
    ['usuarios'=> $usuario]);
    }


    public function create()
    {
        return view('admin.usuarios.cadastrar');
    }


    public function store(Request $request)
    {
        $request->validate([
            'nome' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8',]);

            try {
                $usuario = new User();
                $usuario->nome = $request->nome;
                $usuario->email = $request->email;
                $usuario->perfil =  $request->perfil;
                $usuario->password = bcrypt($request->password);


        if($request->hasFile('imagem')){
            $foto = $request->file('imagem');
            $fotoNomeOriginal = $foto->getClientOriginalName();
            $fotoPath = $foto->storeAs('public/imagem',
            $fotoNomeOriginal);
            $usuario->foto = Storage::url($fotoPath);
        }


                $usuario->save();

                return redirect()->route('admin.usuarios.index')->with('sucesso', 'Usuário cadastrado com sucesso!');
            } catch (\Exception $e) {

                return redirect()->back()->withInput()->with('erro', 'Ocorreu um erro ao cadastrar, por favor tente novamente!');
            }
    }

    public function edit(string $id)
    {
        $usuario = User::findOrFail($id);
        return view('admin.usuarios.editar', [
            'usuario' => $usuario
        ]);
    }


    public function update(Request $request, string $id)
    {
        $usuario = User::findOrFail($id);
        //Validação dos Dados
        $request->validate([
            'nome' => 'required',
            'email' => "required|email|unique:users,id,{$id}",
            'password' => 'sometimes|nullable|min:8',
        ]);


        //Altera os dados na tabela usuarios
        try {
            $usuario->nome = $request->nome;
            $usuario->email = $request->email;
            $usuario->role =  $request->role;

            //Caso o campo senha foi preenchido, a mesma é alterada
            if (!empty($request->password)) {
                $usuario->password = bcrypt($request->password);
            }


        if($request->hasFile('imagem')){
            $iconePath = $request->file('imagem')->store('public/imagem');
            $usuario->icone = Storage::url($iconePath);
        }


            $usuario->save();

            //Caso tudo de certo, o usuário será redireciondado para a view index com uma mensagem
            return redirect()->route('admin.usuarios.index')->with('sucesso', 'Usuário Editado com sucesso!');
        } catch (\Exception $e) {

            return redirect()->back()->withInput()->with('erro', 'Ocorreu um erro ao cadastrar, por favor tente novamente!');
        }
    }


    public function destroy(string $id)
    {
        try {
            User::findOrFail($id)->delete();
            return redirect()->back()->with('sucesso', 'Registro excluído com sucesso!');
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Ocorreu um erro ao excluir, por favor tente novamente!');
        }
    }
}
