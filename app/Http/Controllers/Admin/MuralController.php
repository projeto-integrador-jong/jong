<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Mural;
use App\Models\Parceiro;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class MuralController extends Controller
{

    public function index()
    {
        $murals = Parceiro::paginate(15);
        return view('admin.mural.index',[
            'mural'=> $murals
        ]);

    }

    public function create()
    {
        return view('admin.murals.cadastrar',[

        ]);

    }

    public function store(Request $request)
    {
        $request->validate([
            'titulo' =>'required',
            'descricao' => 'required',

        ]);

        $mural = new Mural();
        $mural->nome = $request->nome;

        $mural->link =$request->link;

        if($request->hasFile('imagem')){
            $imagemPath = $request->file('imagem')->store('public/murals');
            $mural->imagem = Storage::url($imagemPath);

        }
        $mural->save();
        return redirect()->route('admin.parceiros.index')->with('sucesso', 'Parceiros Cadastrado com Sucesso');

    }

    public function edit($id)
    {
        $parceiro = Parceiro::findOrFail($id);

        return view('admin.murals.editar', [
            'parceiro' => $parceiro
        ]);


    }

    public function update(Request $request, $id)
    {

        $request->validate([
            'titulo' =>'required',
            'descricao' => 'required',

        ]);

        $mural = Mural::findOrFail($id);
        $mural->nome = $request->nome;

        $mural->link =$request->link;

        if($request->hasFile('imagem')){
            $imagemPath = $request->file('imagem')->store('public/parceiros');
            $mural->imagem = Storage::url($imagemPath);

        }
        $mural->save();
        return redirect()->route('admin.parceiros.index')->with('sucesso', 'Parceiros Atualzado com Sucesso');

    }

    public function destroy($id)
    {
        $mural = Parceiro::findOrFail($id);

        if($mural->delete()){

            Storage::delete('public/parceiros' . basename($mural->murals));

            return redirect()->route('admin.parceiros.index')->with('sucesso', 'Parceiro Excluido com Sucesso!!');
        }else{

            return redirect()->route('admin.parceiros.editar')->with('erro', 'Houve um erro ao Excluir o registro');

        }

    }


}
