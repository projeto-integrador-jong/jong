<?php

namespace App\Http\Controllers\Admin;

use App\Models\Pet;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class PetController extends Controller
{

    public function index()
    {
        $pets = Pet::paginate(15);
        // dd($Pets);
        return view('admin.pets.index', [
            'pets' => $pets
        ]);
    }

    public function create()
    {

        return view('admin.pets.cadastrar', [
            'pet' => new Pet()
        ]);
    }

    public function store(Request $request)
    {


        $request->validate([
            'nome' => 'required',
            'cor' => 'required',
            'idade' =>'required',
            'sexo' => 'required',
            'imagem' => 'required'
        ]);

        $pet = new Pet();
        $pet->nome = $request->nome;
        $pet->raca = $request->raca;
        $pet->cor = $request->cor;
        $pet->descricao = $request->descricao;
        $pet->idade = $request->idade;
        $pet->sexo = $request->sexo;
        $pet->castrado = $request->castrado ? 1:0;
        $pet->vacinado = $request->vacinado? 1:0;


        if ($request->hasFile('imagem')) {

            //Upload com nome randomico do arquivo
            $petPath = $request->file('imagem')->store('public/pets');
            $pet->imagem = Storage::url($petPath);

        }

        $pet->save();

        return redirect()->route('admin.pets.index')
            ->with('sucesso', 'Pet Cadastrada com Sucesso!');
    }


    public function edit($id)
    {
        $pet = Pet::findOrFail($id);

        return view('admin.pets.editar', [
            'pet' => $pet
        ]);
    }

    public function update(Request $request, $id)
    {

        $request->validate([
            'nome' => 'required',
            'raca' => 'required',
            'cor' => 'required',
            'idade' =>'required',
            'descricao'=> 'required',
            'sexo' => 'required',
            'imagem' => 'required'
        ]);

        $pet = Pet::findOrFail($id);
        $pet->nome = $request->nome;
        $pet->cor = $request->cor;
        $pet->raca = $request->raca;
        $pet->descricao = $request->descricao;
        $pet->idade = $request->idade;
        $pet->sexo = $request->sexo;
        $pet->castrado = $request->castrado? 1:0;
        $pet->vacinado = $request->vacinado? 1:0;


        if ($request->hasFile('imagem')) {

            //Upload com nome randomico do arquivo
            $petPath = $request->file('imagem')->store('public/pets');
            $pet->imagem = Storage::url($petPath);

        }

        $pet->save();

        return redirect()->route('admin.pets.index')
            ->with('sucesso', 'Pet Cadastrada com Sucesso!');
    }

    public function destroy($id)
    {
        $pet = Pet::findOrFail($id);

        if ($pet->delete()) {

            Storage::delete('public/pets/' . basename($pet->pets));

            return redirect()->route('admin.pets.index')
                ->with('sucesso', 'Pet Excluida com Sucesso!');
        } else {

            return redirect()->route('admin.pets.index')
                ->with('erro', 'Houve um erro ao Excluir o registro!');
        }
    }
}
