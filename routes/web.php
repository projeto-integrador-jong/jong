<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\PetController;
use App\Http\Controllers\Site\HomeController;
use Illuminate\Routing\Route as RoutingRoute;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\MuralController;


use App\Http\Controllers\Admin\ContatoController;
use App\Http\Controllers\Admin\UsuarioController;
use App\Http\Controllers\Admin\ParceiroController;
use App\Http\Controllers\Admin\UsuariosController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//rotas site
    Route::get('/',[HomeController::class, 'home'])->name('jong');
    Route::get('/ongs',[HomeController::class, 'ongs'])->name('jong.ongs');
    Route::get('/causa',[HomeController::class, 'causa'])->name('jong.caus');
    Route::get('/doacao',[HomeController::class, 'doacao'])->name('jong.doacao');
    Route::get('/cadastrar',[HomeController::class, 'cadastrar'])->name('jong.cadastrar');
    Route::get('/pet/saude-auau',[HomeController::class, 'petAuau'])->name('jong.saudedoauau');
    Route::get('/pet/saude-miau',[HomeController::class, 'petMiau'])->name('jong.saudemiau');
    Route::get('/pet/petShop',[HomeController::class, 'petShop'])->name('jong.petShop');
    Route::get('/pet/visualizarpet',[HomeController::class, 'visualizarpet'])->name('jong.visualizarpet');
    Route::get('/conheca',[HomeController::class,'conheca'])->name('jong.conheca');
     Route::get('/quemsomos',[HomeController::class,'quemsomos'])->name('jong.quemsomos');
    Route::get('/voluntarios',[HomeController::class,'voluntarios'])->name('jong.voluntarios');



    //rota login
    Route::get('/login',[LoginController::class,'login'])->name('jong.login');
    Route::post('/autenticar',[LoginController::class,'postLogin'])->name('jong.postLogin');
    Route::post('/logout;',[LoginController::class,'logout'])->name('jong.logout');



//rotas admin


Route::get('/admin', function(){
return view('layout.admin');
});

//rotas parceiros

Route::get('/admin/parceiros',[ParceiroController::class,'index'])->name('admin.parceiros.index');
Route::get('/admin/parceiros/cadastrar',[ParceiroController::class, 'create'])->name('admin.parceiros.cadastrar');
Route::post('/admin/parceiros/armazenar',[ParceiroController::class, 'store'])->name('admin.parceiros.armazenar');
Route::get('/admin/parceiros/editar/{id}',[ParceiroController::class, 'edit'])->name('admin.parceiros.editar');
Route::put('/admin/parceiros/atualizar/{id}',[ParceiroController::class, 'update'])->name('admin.parceiros.atualizar');
Route::delete('/admin/parceiros/deletar/{id}',[ParceiroController::class, 'destroy'])->name('admin.parceiros.deletar');

//rotas contatos

Route::get('/admin/contatos',[ContatoController::class,'index'])->name('admin.contatos.index');
Route::get('/admin/contatos/cadastrar',[ContatoController::class,'create'])->name('admin.contatos.cadastrar');
Route::post('/admin/contatos/armazenar',[ContatoController::class,'store'])->name('admin.contatos.armazenar');
Route::get('/admin/contatos/editar/{id}',[ContatoController::class,'edit'])->name('admin.contatos.editar');
Route::put('/admin/contatos/atualizar/{id}',[ContatoController::class,'update'])->name('admin.contatos.atualizar');
Route::delete('/admin/contatos/deletar/{id}',[ContatoController::class,'destroy'])->name('admin.contatos.deletar');


//rotas do pets

Route::get('/admin/pets',[PetController::class,'index'])->name('admin.pets.index');
Route::get('/admin/pets/cadastrar',[PetController::class, 'create'])->name('admin.pets.cadastar');
Route::post('/admin/pets/armazenar',[PetController::class, 'store'])->name('admin.pets.armazenar');
Route::get('/admin/pets/editar/{id}',[PetController::class, 'edit'])->name('admin.pets.editar');
Route::put('/admin/pets/atualizar/{id}',[PetController::class, 'update'])->name('admin.pets.atualizar');
Route::delete('/admin/pets/delatar/{id}',[PetController::class, 'destroy'])->name('admin.pets.deletar');

//rota do usuario

Route::get('/admin/usuarios', [UsuarioController::class, 'index'])->name('admin.usuarios.index');
Route::get('admin/usuarios/cadastar',[UsuarioController::class, 'create'])->name('admin.usuarios.cadastrar');
Route::post('admin/usuarios/armazenar',[UsuarioController::class, 'store'])->name('admin.usuarios.armazenar');
Route::get('admin/usuarios/editar/{id}',[UsuarioController::class, 'edit'])->name('admin.usuarios.editar');
Route::put('admin/usuarios/atualizar/{id}',[UsuarioController::class, 'update'])->name('admin.usuarios.atualizar');
Route::delete('admin/usuarios/deletar/{id}',[UsuarioController::class, 'destroy'])->name('admin.usuarios.deletar');


//rota do artigo

Route::get('/admin/artigos', [ArtigoController::class, 'index'])->name('admin.artigos.index');
Route::get('/admin/artigos/cadastrar',[ArtigoController::class, 'create'])->name('admin.artigos.cadastrar');
Route::post('/admin/artigos/armazenar',[ArtigoController::class, 'store'])->name('admin.artigos.armazenar');
Route::get('/admin/artigos/editar/{id}',[ArtigoController::class, 'edit'])->name('admin.artigos.editar');
Route::put('/admin/artigos/atualizar/{id}',[ArtigoController::class, 'update'])->name('admin.artigos.atualizar');
Route::get('/admin/artigos/delete/{id}',[ArtigoController::class, 'destroy'])->name('admin.artigos.deletar');

//rota do mural

Route::get('/admin/mural', [MuralController::class, 'index'])->name('admin.mural.index');
