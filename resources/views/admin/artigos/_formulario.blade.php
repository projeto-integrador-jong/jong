@csrf

@if ($errors->any())

    <ul class="alert alert-danger p-2 list-unstyled">
        @foreach ($errors->all() as $erro)
            <li>{{ $erro }}</li>
        @endforeach
    </ul>

@endif

<div class="col-md-12">
    <label for="titulo"
           class="form-label">Título</label>
    <input type="text"
           class="form-control @error('titulo') is-invalid

           @enderror"
           name="titulo"
           id="titulo"
           placeholder="Insira o Título"
           value="{{old('titulo', $artigo->titulo)}}">

</div>

<div class="col-md-12">
    <label for="texto"
           class="form-label">Texto</label>
           <textarea
           class="form-control @error('texto') is-invalid

           @enderror"
           name="texto"
           id='texto'
           rows="10">{{old('texto', $artigo->texto)}}</textarea>
</div>

<div class="col-md-12">
    @if ($artigo->iamgem)
        <img src="{{ $artigo->imagem }}"
             alt=""
             width="100">
    @endif
</div>

<div class="col-md-12">
    <label for="imagem"
           class="form-label">Imagem</label>
    <input type="file"
           class="form-control @error('foto') is-invalid

           @enderror"
           name="imagem"
           id="imagem">


</div>


<div class="col-12">
    <button type="submit"
            class="btn btn-warning ">Salvar</button>
</div>

