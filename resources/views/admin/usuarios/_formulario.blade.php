@csrf

@if ($errors->any())
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    @endif

<div class="col-md-12">
    <label for="nome"
           class="form-label">Nome</label>
    <input type="text"
           class="form-control @error('nome') is-invalid @enderror"
           name="nome"
           id="nome"
           placeholder="Insira o Nome"
           value="{{ old('nome') }}">

           @error('nome')
           <div class="invalid-feedback">
               {{ $message }}
           </div>
       @enderror

</div>
<div class="col-md-12">
    <label for="email"
           class="form-label">E-mail</label>
    <input type="text"
           name="email"
           class="form-control @error('email') is-invalid @enderror"
           id="email"
           placeholder="Insira a E-mail"
           value="{{ old('email') }}">

           @error('email')
           <div class="invalid-feedback">
               {{ $message }}
           </div>
       @enderror

</div>
<div class="col-md-12">
    <label for="senha"
           class="form-label">Senha</label>
    <input type="password"
           name="password"
           class="form-control @error('senha') is-invalid @enderror"
           id="password"
           placeholder="">

           @error('senha')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
    @enderror

</div>
<div class="col-md-3">
    <label for="perfil"
           class="form-label">Perfil</label>
    <select class="form-control"
            id="perfil"
            name="perfil">
        <option value="cliente" {{ old('role') == 'Cliente' ? 'selected' : '' }}>Cliente</option>
        <option value="administrador" {{ old('role') == 'Administrador' ? 'selected' : '' }}>Administrador</option>
    </select>
</div>

<div class="col-md-12">
    <label for="Avatar"
           class="form-label">Avatar</label>
    <input type="file"
           class="form-control"
           name="imagem"
           id="imagem" >



</div>

<div class="col-12">
    <button type="submit"
            class="btn btn-primary">Salvar</button>
</div>
