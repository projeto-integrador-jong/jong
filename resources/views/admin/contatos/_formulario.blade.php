@csrf

@if ($errors->any())

    <ul class="alert alert-danger p-2 list-unstyled">
        @foreach ($errors->all() as $erro)
            <li>{{ $erro }}</li>
        @endforeach
    </ul>

@endif

<div class="col-md-12">
    <label for="nome" class="form-label">Nome</label>
    <input type="text" class="form-control @error('nome') is-invalid

           @enderror" name="nome" id="nome"
        placeholder="Insira o Nome" value="{{ old('nome', $contatos->nome) }}">

</div>

<div class="col-md-6">
    <label for="email" class="form-label">Email</label>
    <input type="text" class="form-control @error('email') is-invalid @enderror" name="email" id="email">

</div>

<div class="col-md-6">
    <label for="telefone" class="form-label">Telefone</label>
    <textarea class="form-control @error('telefone') is-invalid

           @enderror" name="telefone" id='telefone'
        rows="1">{{ old('telefone', $contatos->telefone) }}</textarea>
</div>

<div class="col-md-6">
    <label for="mensagem" class="form-label">Mensagem</label>
    <textarea class="form-control @error('mensagem') is-invalid

           @enderror" name="mensagem" id='mensagem'
        rows="10">{{ old('mensagem', $contatos->mensagem) }}</textarea>
</div>


<div class="col-12">
    <button type="submit" class="btn btn-warning ">Salvar</button>
</div>
