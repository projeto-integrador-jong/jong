@csrf

@if ($errors->any())

    <ul class="alert alert-danger p-2 list-unstyled">
        @foreach ($errors->all() as $erro)
            <li>{{ $erro }}</li>
        @endforeach
    </ul>

@endif

<div class="col-md-12">
    <label for="nome"
           class="form-label">Nome</label>
    <input type="text"
           class="form-control @error('nome') is-invalid @enderror"
           name="nome"
           id="nome"
           placeholder="Insira o Nome"
           value="{{ old('nome', $parceiro->nome) }}">

</div>

<div class="col-md-12">
    @if ($parceiro->imagem)
        <img src="{{ $parceiro->imagem }}"
             alt=""
             width="100">
    @endif
</div>

<div class="col-md-12">

    <label for="imagem"
           class="form-label">Imagem</label>
    <input type="file"
           class="form-control @error('imagem') is-invalid @enderror"
           name="imagem"
           id="imagem">

</div>

<div class="col-md-12">
    <label for="link"
           class="form-label">Link</label>
    <input type="text"
           class="form-control @error('link') is-invalid @enderror"
           name="link"
           id="link"
           placeholder="Insira o Link"
           value="{{ old('link', $parceiro->link) }}">

</div>


<div class="col-12">
    <button type="submit"
            class="btn btn-primary">Salvar</button>
</div>
