@csrf

@if ($errors->any())

    <ul class="alert alert-danger p-2 list-unstyled">
        @foreach ($errors->all() as $erro)
            <li>{{ $erro }}</li>
        @endforeach
    </ul>

@endif

<div class="col-md-12">
    <label for="nome"
           class="form-label">Nome</label>
    <input type="text"
           class="form-control @error('nome') is-invalid

           @enderror"
           name="nome"
           id="nome"
           placeholder="Insira o Nome"
           value="{{old('nome', $pet->nome)}}">

</div>

<div class="col-md-12">
    <label for="raca"
           class="form-label">Raça</label>
    <input type="text"
           class="form-control @error('raca') is-invalid

           @enderror"
           name="raca"
           id="raca"
           placeholder="Insira o raca"
           value="{{old('raca', $pet->raca)}}">

</div>

<div class="col-md-12">
    <label for="cor"
           class="form-label">Cor</label>
    <input type="text"
           class="form-control @error('cor') is-invalid

           @enderror"
           name="cor"
           id="cor"
           placeholder="Insira o cor"
           value="{{old('cor', $pet->cor)}}">

</div>


<div class="col-md-3">
    <label for="idade"
           class="form-label">Idade</label>
    <input type="text"
           class="form-control @error('idade') is-invalid @enderror"
           name="idade"
           id="idade">

</div>

<div class="col-md-3">
    <label for="sexo"
           class="form-label">Sexo</label>
    <select class="form-control"
            id="sexo"
            name="sexo">
        <option value="Macho" {{ old('sexo') == 'Macho' ? 'selected' : '' }}>Macho</option>
        <option value="Fêmea" {{ old('sexo') == 'Fêmea' ? 'selected' : '' }}>Femea</option>
    </select>
</div>

<div class="col-md-12">
    <label for="descricao"
           class="form-label">Descrição</label>
           <textarea name="descricao" class="form-control"> {{ old('descricao', $pet->descricao)}}</textarea>

</div>

<div class="form-check">
    <input class="form-check-input" name="vacinado" type="checkbox"  id="flexCheckDefault" @if ($pet->vacinado)checked @endif>
    <label class="form-check-label" for="flexCheckDefault">
      Vacinado
    </label>
  </div>

  <div class="form-check">
    <input class="form-check-input" type="checkbox" name="castrado"  id="flexCheckChecked" @if ($pet->castrado)checked

    @endif>
    <label class="form-check-label" for="flexCheckChecked">
      Castrado
    </label>
  </div>


  <div class="col-md-12">
    @if ($pet->imagem)
        <img src="{{ asset($pet->imagem) }}"
             alt=""
             width="100">
    @endif
</div>
<div class="col-md-12">

    <label for="imagem"
           class="form-label">Imagem</label>
    <input type="file"
           class="form-control @error('imagem') is-invalid @enderror"
           name="imagem"
           id="imagem">

</div>


<div class="col-12">
    <button type="submit"
            class="btn btn-warning ">Salvar</button>
</div>

