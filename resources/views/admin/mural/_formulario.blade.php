@csrf

@if ($errors->any())

    <ul class="alert alert-danger p-2 list-unstyled">
        @foreach ($errors->all() as $erro)
            <li>{{ $erro }}</li>
        @endforeach
    </ul>

@endif

<div class="col-md-12">
    <label for="titulo"
           class="form-label">Título</label>
    <input type="text"
           class="form-control @error('titulo') is-invalid

           @enderror"
           name="titulo"
           id="titulo"
           placeholder="Insira o Título"
           value="{{old('titulo', $mural->titulo)}}">

</div>



<div class="col-md-12">
    <label for="descricao"
           class="form-label">Descrição</label>
           <textarea
           class="form-control @error('descricao') is-invalid

           @enderror"
           name="descricao"
           id='descricao'
           rows="10">{{old('descricao', $mural->descricao)}}</textarea>
</div>






<div class="col-12">
    <button type="submit"
            class="btn btn-warning ">Salvar</button>
</div>

