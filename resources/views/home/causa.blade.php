@extends('layout.site')

@section('titulo', 'Conheça a Causa')

@section('conteudo')


    <div id="tituloPagina">
        <h2>Conheça nossa Ong</h2>
    </div>
    <div class="container">
        <div class="row">
            <div id="tex"class="col-md-6">
                <p>Somos o grupo Jong (junção das ongs), somos uma equipe onde nos preocupamos com as dificuldades
                    que
                    as ongs da cidade de Marilia-SP possuem quando o assunto é adoção de gatos e cachorros.</p>
                <p>E através do Projeto Integrador realizado no Senac-Marilia. Pensamos: “Por que não criar um site
                    onde
                    concentraria todas as ongs da cidade? Assim, a população teria um meio rápido e fácil para
                    entrar em
                    contato e visualizar os animais.” </p>
                <p>Por esse motivo, para início selecionamos algumas ongs para iniciar esse projeto, para que fossem
                    incluídas e participassem e apoiassem esse começo. </p>
            </div>

            <div id="tex" class="col-md-6">
                <img src="img/dogtiti.png " class="img-fluid" alt="dog">
            </div>
        </div>
    </div>


@endsection
