@extends('layout.site')

@section('titulo', 'Conheça a Causa')

@section('conteudo')

    <div id="tituloPagina">
        <h2>Quem Somos</h2>
    </div>
    <div class="container">
        <div class="row">
            <div id="tex"class="col-md-6">
                <p>O projeto foi criado pelos alunos Juliana Alves, Ana Ferreira, Reginaldo Rodrigues e Guilherme Boassali.
                    Visualizamos a necessidade de centralizar as ongs da cidade, gerando a facilidade de propagar e
                    apresentar os animais a população mariliense.Se você está aqui, pedimos que apoie a causa, adotando,
                    realizando doações, compartilhando, sendo voluntário... Enfim, toda ajuda é bem-vinda. Gratidão pela
                    visita</p>
            </div>

            <div id="tex" class="col-md-6">
                <img src="/img/reginaldo.png" class="img-fluid" alt="reginaldo">
            </div>
        </div>
    </div>

@endsection
