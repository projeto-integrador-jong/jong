@extends('layout.site')

@section('titulo', 'Home')

@section('conteudo')




    <div id="carouseBanner" class="carousel slide" data-bs-ride="carousel">

        <div class="carousel-inner">
            <div id="demo" class="carousel slide" data-bs-ride="carousel">
                <div class="carousel-indicators">
                    <button type="button" data-bs-target="#demo" data-bs-slide-to="0" class="active"></button>
                    <button type="button" data-bs-target="#demo" data-bs-slide-to="1"></button>
                    <button type="button" data-bs-target="#demo" data-bs-slide-to="2"></button>
                </div>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="img/banner1.png" alt="banner cachorro" class="d-block" style="width:100%">
                    </div>
                    <div class="carousel-item">
                        <img src="img/banner2.png" alt="banner cão" class="d-block" style="width:100%">
                    </div>

                    <div class="carousel-item">
                        <img src="img/banner3.png" alt="banner gato" class="d-block" style="width:100%">
                    </div>
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#demo" data-bs-slide="prev">
                    <span class="carousel-control-prev-icon"></span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#demo" data-bs-slide="next">

                    <span class="carousel-control-next-icon"></span>
                </button>
            </div>
        </div>





        <div id="mineBanner">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-6 col-md-3">
                        <a href="{{ route('jong.saudedoauau') }}" target="blanck"><img src="/img/auau.png" class="img-fluid"
                                alt="saude do au au"></a>
                    </div>
                    <div class="col-md-3 col-6 col-md-3">
                        <a href="{{ route('jong.saudemiau') }}" target="blanck"><img src="/img/miau.png" class="img-fluid"
                                alt="saude do miau"></a>
                    </div>
                    <div class="col-md-3 col-6 col-md-3">
                        <a href="{{ route('jong.petShop') }}" target="blanck"><img src="/img/petshopp.png" class="img-fluid"
                                alt="petshops"></a>
                    </div>
                    <div class="col-md-3 col-6 col-md-3">
                        <a href="{{ route('jong.doacao') }}" target="blanck"><img src="/img/totodoa.png" class="img-fluid"
                                alt="doação"></a>
                    </div>

                </div>
            </div>
        </div>

        <section id="fotoPet">
            <div id="vejaNossosPets">
                <div class="container">
                    <h2>Adote um Pet</h2>
                </div>

            </div>
            <div class="container">
                <div class="tamanhoPet ">
                    <div class="row imagensPets">

                        @foreach ($pets as $pet)
                            <div class="col-md-3 col-6 text-center">
                                <a href="{{ route('jong.visualizarpet') }}" target="blanck"><img src="{{ $pet->imagem }}"
                                        class="img-fluid" alt="pet"></a>
                                <h3>{{ $pet->nome }}</h3>
                                <p>{{ $pet->idade }}</p>
                            </div>
                        @endforeach


                    </div>
                </div>


            </div>
        </section>

        <section id="videoPrimeiro">
            <div class="container">

                <div class="row">
                    <div class="col-md-6 d-none d-lg-block">
                        <video width="500" height="400" autoplay loop="loop" muted="muted">
                            <source src="/img/cachorro_filhote.mp4" type="">
                            Seu navegador não é compatível
                        </video>
                    </div>
                    <div class="col-md-6 d-none d-lg-block">
                        <h2 class="descricao">Veja um pouco do nosso dia a dia</h2>
                        <p>Por trás de tantos carinhos, abrigo e comida, as ongs possui um grande trabalho.
                            Todo o processo de limpeza, gerenciamento financeiro, processo de adoção, feiras e rifas para
                            arrecadação de dinheiro também fazem parte da organização da mesma.

                        <p class="negrito">Assista e saiba um pouco do dia a dia dos voluntários.</p>
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <div id="listaOng"></div>

        <section>

            <div id="juncaoOngs">

                <div class="container">

                    <div class="row justify-content-md-center">
                        <h2 class="parceiros"> Nossos Parceiros </h2>
                        <div class="col-md-3 col-6">
                            <img src="img/11.png" class="img-fluid" alt="Ong amor animal">
                        </div>

                        <div class="col-md-3 col-6">
                            <img src="img/garra.png" class="img-fluid" alt="Ong Garra">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="videoSegundo">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 d-none d-lg-block">
                        <h2 class="descricao">Conheça alguns dos pets adotados</h2>
                        <p>“Adotamos um ao outro, simples assim: porque nós precisávamos dele e ele de nós”.

                            Essa é uma frase da família Ferreira, que no ano de 2021 adotaram o Tody, um gatinho “vira-lata”
                            que chegou para somar e alegrar a familia.

                        <p class="negrito">Saiba como foi essa chegada e como ele está hoje. </p>
                        </p>
                    </div>
                    <div class="col-md-6 d-none d-lg-block">
                        <video width="600" height="400" autoplay loop="loop" muted="muted">
                            <source src="/img/gato_filhote.mp4" type="">
                            Seu navegador não é compatível
                        </video>
                    </div>
                </div>
            </div>
        </section>

        <section>
            <div id="bannerContato">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 d-none d-lg-block">
                            <img src="/img/dog3.jpg" width="500" alt="cachorro">
                        </div>

                        <div id="formContato" class="col-md-6">
                            <h2 class="pergunta">Faça uma Pergunta!</h2>
                            <form action="enviar.php" method="post">
                                <p>
                                    <input type="email" name="email" placeholder="E-mail:">
                                </p>

                                <p>
                                    <input type="tel" name="tel" placeholder="Tel:">
                                </p>

                                <p>
                                    <textarea name="duvida" rows="5" placeholder="Mensagem..."></textarea>
                                </p>

                                <p>
                                    <button type="submit" class="btn btn-dark ">Enviar</button>
                                </p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>


    @endsection
