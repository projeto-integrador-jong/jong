@extends('layout.site')

@section('titulo', 'Saude do Miau')

@section('conteudo')

    <div id="tituloPagina">
        <h2>Intoxicação em gatos: saiba como prevenir o problema</h2>
    </div>
    <div class="container">
        <div class="row">
            <div id="tex"class="col-md-6">
                <p>A intoxicação é um processo natural, que ocorre quando o corpo reage a uma substância nociva. Ela pode
                    ocorrer tanto por ingestão como por um simples contato com determinado produto. Trata-se de algo muito
                    comum tanto em humanos como animais.</p>
                <p>Porém, a intoxicação em pets pode ser um pouco mais complicada. Afinal, os gatos são bem menores do que
                    os seres humanos, e a ingestão de algo tóxico pode causar sérios problemas. Portanto, fique de olho em
                    substâncias perigosas para os bichanos.</p>
            </div>

            <div id="tex" class="col-md-6">
                <img src="/img/tec.jpg" class="img-fluid" alt="saude do miau">
            </div>
        </div>
    </div>



@endsection
