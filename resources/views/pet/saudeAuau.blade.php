@extends('layout.site')

@section('titulo', 'Saude do Au-Au')

@section('conteudo')



    <div id="tituloPagina">
        <h2>MEU PET COMEU ALGO ESTRANHO. E AGORA?</h2>
    </div>
    <div class="container">
        <div class="row">
            <div id="tex"class="col-md-6">
                <p>Cães têm uma curiosidade natural e muitas vezes acabam ingerindo o que não devem.</p>
                <p>A ingestão de objetos (corpos estranhos, como são denominados no meio veterinário) é uma ocorrência comum
                    e o perigo para os animais de estimação é grande.</p>
                <p>Os cães engolem mais objetos estranhos que os gatos, porque não são muito seletivos. Eles comem de tudo:
                    bola de tênis, bola de golfe, osso, corrente, moeda, papel, pedaços de madeira, controle remoto, meia,
                    roupas, lápis, rodapé, porta, carpete, poltrona, telefone celular, etc. A lista é muito longa e não é
                    possível enumerar todos os objetos que já foram ingeridos por cães.</p>
                <p>Apesar de bastante comum, essa ingestão de corpos estranhos pode se tornar um problema grave. Se o objeto
                    não for pontiagudo e for ingerido por um cachorro grande, provavelmente será eliminado através das fezes
                    sem maiores problemas. Mas, em alguns casos, pode acontecer do objeto se alojar em alguma porção do
                    trato gastrointestinal que seja mais estreita e o animal não conseguir eliminá-lo. Nesses casos, o trato
                    gastrointestinal fica obstruído e o animal começa a apresentar sintomas.</p>

            </div>

            <div class="col-md-6 saudeau">
                <img src="/img/tonto.jpg" class="img-fluid" alt="">
            </div>
        </div>
    </div>

@endsection
