<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('titulo') - junção das Ongs</title>
    <meta name="description" content="Jong junção das Ong onde você pode adotar e dor seu pet">
    <meta name="keywords" content="Doação, Gato, Cachorro, cudados, dicas de veterinário e etc">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">

    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">

    <link rel="stylesheet" href="/css/main.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Varela+Round&display=swap" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>


</head>

<body>
    <header>
        <div id="barraSuperior" class="topo fontes">
            <div class="container">
                <div class="row">
                    <div id="logotipo" class="col-md-2 col-10">
                        <a href="{{ route('jong') }}"><img src="/img/logojong.png" alt="jongjuncaodasongs"
                                class="img-fluid"></a>
                    </div>

                    <div class="col-2 d-block d-lg-none">
                        <button id="btnMenu" class="btn btn mt-5">
                            <i class="fas fa-bars"></i>
                        </button>
                    </div>

                    <nav id="menu" class="col-md-10 color">
                        <ul>
                            <li><a href=" {{ route('jong.ongs') }}" alt="ongs">Ongs</a></li>
                            <li><a href="{{ route('jong.voluntarios') }}" alt="seja voluntario"> Voluntários</a></li>
                            <li><a href=" {{ route('jong.conheca') }}"alt="conheça a causa">Conheça a causa</a></li>
                            <li><a href="{{ route('jong.doacao') }}" alt="fça doação">Faça sua doação</a></li>
                            <li><a href=" {{ route('jong.login') }}" alt="login">Login</a></li>

                            {{-- <form class="d-flex" role="search">
                                <input class="form-control me-2" type="search" placeholder="Buscar"
                                    aria-label="Buscar">
                                <button class="btn btn-outline-success" type="submit">Buscar</button>
                            </form> --}}
                        </ul>
                    </nav>

                </div>

            </div>

        </div>
        </div>


        <main>

            @yield('conteudo')

        </main>

        <footer>
            <div id="rodape">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <h2>Institucional</h2>
                            <ul>
                                <li><a href="{{ route('jong.quemsomos') }}">Quem Somos</a></li>
                                <li>Sobre as Ongs</li>
                                <li><a href="sobreosvoluntarios.html">Sobre os Voluntários</a></li>
                            </ul>
                        </div>

                        <div class="col-md-3">
                            <h2>Redes Sociais</h2>
                            <p><i class="fa-brands fa-facebook-f"></i> Jong junção das ongs</p>
                            <p><i class="fa-brands fa-instagram"></i> jongjuncaodasongs</p>
                            <p><i class="fa-brands fa-whatsapp-square"></i> (14) 99839-8799</p>
                        </div>

                        <div class="col-md-3">
                            <h2>Contatos</h2>
                            <p>Telefone</p>
                            <p>E-mail</p>
                        </div>

                        <div class="col-md-3">
                            <h2>Segurança</h2>
                            <img src="/img/Selodequalidade.png" width="80">
                            <img src="/img/SeloSSL.png" width="80">
                        </div>
                    </div>
                </div>
            </div>


            <div class="copyright">
                <p>
                    &copy; Todos os direitos são resevados. Desenvolvido por Reginaldo, Juliana, Ana e Guilherme.
                </p>
            </div>
        </footer>

        <!-- JavaScript Bundle with Popper -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous">
        </script>

        <script src="js/main.js"></script>

</body>

</html>
