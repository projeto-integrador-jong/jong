<?php

namespace Database\Seeders;

use App\Models\Artigo;
use App\Models\Parceiro;
use Faker\Factory as Faker;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ParceiroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('pt_BR');

        foreach(range(1,10) as $index){

            Parceiro::create([

                'nome' => $faker->company(),
                'link' => $faker->text(),
                'imagem' =>'storage/parceiros/parceiro.jpg',


            ]);}
    }
}
