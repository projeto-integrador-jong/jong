<?php

namespace Database\Seeders;

use App\Models\Pet;
use App\Models\Artigo;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class PetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('pt_BR');

        foreach(range(1,10) as $index){

            Pet::create([

                'nome' => $faker->firstName(),
                'raca' =>$faker->firstName(),
                'cor' => $faker->colorName(),
                'descricao' => $faker->text(),
                'idade' => $faker->numberBetween(0,14),
                'sexo' => "Macho",
                'imagem' =>'storage/pets/pet.jpg',


            ]);}
    }
}
