<?php

namespace Database\Seeders;

use App\Models\Artigo;
use Faker\Factory as Faker;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ArtigoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('pt_BR');

        foreach(range(1,10) as $index){

            Artigo::create([

                'titulo' => $faker->title(),
                'descricao' => $faker->text(),
                'imagem' =>'storage/artigos/artigo.jpg',


            ]);}
    }
}
