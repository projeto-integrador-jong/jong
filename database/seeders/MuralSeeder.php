<?php

namespace Database\Seeders;

use App\Models\Mural;
use App\Models\Artigo;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class MuralSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('pt_BR');

        foreach(range(1,10) as $index){

            Mural::create([

                'titulo' => $faker->title(),
                'descricao' => $faker->text(),


            ]);}
    }
}
