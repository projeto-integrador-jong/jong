<?php

namespace Database\Seeders;
use Faker\Factory as Faker;
use App\Models\Contato;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ContatoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('pt_BR');

        foreach(range(1,10) as $index){

            Contato::create([
                'nome' => $faker->name,
                'email' => $faker->unique()->safeEmail(),
                'telefone' => $faker->phoneNumber(),
                'mensagem' =>$faker->text(),


            ]);}

    }
}
