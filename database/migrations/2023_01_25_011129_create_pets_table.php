<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pets', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('nome',45);
            $table->string('raca');
            $table->string('cor');
            $table->string('imagem');
            $table->string('descricao',500);
            $table->string('idade',45);
            $table->string('sexo',6);
            $table->boolean('castrado')->default(0);
            $table->boolean('vacinado')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pets');
    }
};
