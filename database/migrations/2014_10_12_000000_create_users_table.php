<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {

            $table->id();

            $table->string('nome', 80);
            $table->string('password');
            $table->string('email',50);
            $table->string('telefone', 16)->nullable();  ;
            $table->string('responsavel', 50)->nullable();  ;
            $table->string('cpf_cnpj',45)->nullable();  ;
            $table->string('perfil', 45)->default('voluntario');
            $table->string('endereco', 100)->nullable();  ;
            $table->string('cep',45)->nullable();  ;
            $table->string('complemento',45)->nullable();  ;
            $table->string('bairro',45)->nullable();  ;
            $table->string('cidade', 45)->nullable();  ;
            $table->string('estado',45)->nullable();  ;
            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->timestamps();



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};

